import Query from './library/query'
import Schema from './library/schema'
import Collection from './library/collection'
import Document from './library/document'
import Datastore from './library/datastore'

export default {
	Query,
	Schema,
	Collection,
	Document,
	Datastore
}
