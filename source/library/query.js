import R from 'ramda'
import pluralize from 'plur'
import Debugger from 'debug'

const debug = Debugger('rodb:query')

const _ = R.__

const query = {
	from: 'people',
	get: '*',
	where: {
		age: {
			gte: 18,
			lt: 40
		},
		name: 'James'
	}
}

const convo = (value, key, object) => {
	if (typeof object !== 'object') {
		return { equals: object }
	} else {
		return object
	}
}

const convert = query => {
	let convert = (value, key, obj) => {
		if (/(l|g)t(e?)/.test(key)) {
			debug('needs curry')
			return R[key](_, value)
		} else {
			debug('doesnt needs curry')
			return R[key](value)
		}
	}

	console.log(query)

	let funs = R.values(R.mapObjIndexed(convert, query))
	return R.allPass(funs)
}

const f = convert(query.where.age)
/*for (let i = 1; i<50; i++) {
	debug(f(i))
}*/

